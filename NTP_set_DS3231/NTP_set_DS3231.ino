#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

/* Defining configuration I don't want in git seperate. 
 * Create a file in the same directory with the following:
 *    const char wlan_ssid[] = "<ssid_value>";
 *    const char wlan_pass[] = "<wifi_password>";
 *    
 * Adding the same variable declarations with "extern" will 
 * allow the compiler to find the correct values from the 
 * other file.
 * 
 */
extern const char wlan_ssid[];
extern const char wlan_pass[];


WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

void setup() {
  Serial.begin(9600);
 
  while (!Serial) delay(1);

  connectWiFi();
  timeClient.begin();
}

void loop() {
  timeClient.update();

  Serial.println(timeClient.getFormattedTime());

  delay(1000);
}

void connectWiFi(void) {

  Serial.print("Attempting connection to ");
  Serial.println(wlan_ssid);

  WiFi.begin(wlan_ssid, wlan_pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(F("."));
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); 
  Serial.println(WiFi.localIP());

}
